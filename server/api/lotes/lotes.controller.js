import Lote from './lotes.model';
require('lodash');
//WARNIG: IT WILL DROP THE TABLE
exports.resetLote = (req, res) => {
  Lote.sync({ force: true })
    .then(() => {
      // Table created
      res.sendStatus(200);
    });
}
exports.createLote = (req, res) => {
  /* console.log ('Si entra aqui');
   Lote.findOne({
       where: { nombre: req.body.nombre }
     })
   .then((terreno) => {
     if (terreno) {
       res.status(200)
       .send('El terreno ya existe');
     }
     else {
       Terreno.create(req.body)
       .then(() => {
         res.sendStatus(200);
       })
       .catch((err) => {
         console.log(err);
         res.sendStatus(500);
       })
     }
   })
 }/ */


  Lote.create(req.body)
    .then(() => {
      res.sendStatus(200);
    })
    .catch((err) => {
      console.log(err);
      res.sendStatus(500);
    });
}

exports.modifyLote = (req, res) => {/*
    Lote.findOne({
        where: { nombre: req.body.nombre }
      })
    .then((Terreno) => {
      if (Terreno) {
        res.status(200)
        .send('El usuario ya existe');
      }
      else {
       */ Lote.findOne({
    where: { id: req.body.id }
  })
    .then((Lote) => {
      if (Lote) {
        Lote.update(req.body)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
  /* }
 })*/
}

exports.deleteLote = (req, res) => {
  Lote.findOne({
    where: { id: req.body.id }
  })
    .then((lote) => {
      if (lote) {
        lote.destroy()
          .then(lote => {
            res.status(200)
              .json(lote.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.findLote = (req, res) => {
  Lote.findAll()
    .then((lotes) => {

      res.send(lotes);
    })
}

exports.setTerreno = (req, res) => {

  Lote.findOne({
    where: { id: req.body.id }
  })
    .then((Lote) => {
      if (Lote) {
        Lote.setTerreno(req.body.TerrenoId)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.getTerreno = (req, res) => {
  
  Lote.findOne({
    where: { id: req.body.id }
  }).then((lote) => {
   

    lote.getTerreno()
      .then((terreno) => {
        res.send(terreno);
      })

    console.log(lote);
  })
}

exports.deleteTerreno = (req, res) => {
  Lote.findOne({
    where: { id: req.body.id }
  })
    .then((lote) => {
      if (lote) {
        lote.setTerreno(null)
          .then(lote => {
            res.status(200)
              .json(lote.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })

}