import sequelizeDb from '../../config/db';
import Sequelize from 'sequelize';
import Terreno from '../terrenos/terrenos.model'

var Lote = sequelizeDb.define('Lote', {
    nombre: {
        allowNull: false,
        type: Sequelize.STRING
    },
    descripcion: {
        allowNull: false,
        type: Sequelize.STRING
    },
    area: {
        allowNull: false,
        type: Sequelize.INTEGER
    },
    precio: {
        allowNull: false,
        type: Sequelize.INTEGER
    },
    
});

Lote.belongsTo(Terreno, {as:'Terreno'}); 

module.exports = Lote;