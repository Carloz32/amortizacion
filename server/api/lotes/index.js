import controller from './lotes.controller';
var router = require('express').Router();

router.route('/')
.post(controller.createLote)
.get(controller.findLote)
.put(controller.modifyLote)

router.route('/delete')
.put(controller.deleteLote)

router.route('/model')
.delete(controller.resetLote)

router.route('/relation')
.post(controller.setTerreno)
.put(controller.getTerreno)
.delete(controller.deleteTerreno)



module.exports = router;