import controller from './terrenos.controller';
var router = require('express').Router();
router.route('/')
.post(controller.createTerreno)
.get(controller.findTerreno)
.put(controller.modifyTerreno)

router.route('/delete')
.put(controller.deleteTerreno)

router.route('/model')
.delete(controller.resetTerreno)

module.exports = router;