import sequelize from '../../config/db';
import Sequelize from 'sequelize';


var Terreno = sequelize.define('Terreno', {
    nombre: {
        allowNull: false,
        type: Sequelize.STRING
    },
    ubicacion: {
        allowNull: false,
        type: Sequelize.STRING
    },
    imagen: {
        allowNull: false,
        type: Sequelize.STRING
    },
    area: {
        allowNull: false,
        type: Sequelize.STRING
    }
});


module.exports = Terreno;