import sequelizeDb from '../../config/db';
import Sequelize from 'sequelize';


var Interes = sequelizeDb.define('Interes', {
    fechaInicio: {
        allowNull: false,
        type: Sequelize.DATEONLY
    },
    fechaTermino: {
        allowNull: false,
        type: Sequelize.DATEONLY
    },
    porcentajeInteres: {
        allowNull: false,
        type: Sequelize.FLOAT
    }
    
});


module.exports = Interes;