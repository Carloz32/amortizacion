import controller from './intereses.controller';
var router = require('express').Router();

router.route('/')
.post(controller.createInteres)
.get(controller.findInteres)
.put(controller.modifyInteres)

router.route('/delete')
.put(controller.deleteInteres)

router.route('/model')
.delete(controller.resetInteres)



module.exports = router;