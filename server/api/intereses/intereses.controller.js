import Interes from './intereses.model';
require('lodash');
//WARNIG: IT WILL DROP THE TABLE
exports.resetInteres = (req, res) => {
  Interes.sync({ force: true })
    .then(() => {
      // Table created
      res.sendStatus(200);
    });
}
exports.createInteres = (req, res) => {
  /* console.log ('Si entra aqui');
   Lote.findOne({
       where: { nombre: req.body.nombre }
     })
   .then((terreno) => {
     if (terreno) {
       res.status(200)
       .send('El terreno ya existe');
     }
     else {
       Terreno.create(req.body)
       .then(() => {
         res.sendStatus(200);
       })
       .catch((err) => {
         console.log(err);
         res.sendStatus(500);
       })
     }
   })
 }/ */
  Interes.create(req.body)
    .then(() => {
      res.sendStatus(200);
    })
    .catch((err) => {
      console.log(err);
      res.sendStatus(500);
    });
}

exports.modifyInteres = (req, res) => {/*
    Lote.findOne({
        where: { nombre: req.body.nombre }
      })
    .then((Terreno) => {
      if (Terreno) {
        res.status(200)
        .send('El usuario ya existe');
      }
      else {
       */ Interes.findOne({
    where: { id: req.body.id }
  })
    .then((Interes) => {
      if (Interes) {
        Interes.update(req.body)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
  /* }
 })*/
}

exports.deleteInteres = (req, res) => {
  Interes.findOne({
    where: { id: req.body.id }
  })
    .then((interes) => {
      if (interes) {
        interes.destroy()
          .then(interes => {
            res.status(200)
              .json(interes.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.findInteres = (req, res) => {
  Interes.findAll()
    .then((intereses) => {
      res.send(intereses);
    })
}

