var router = require('express').Router();


router.use('/lotes', require('./lotes'));
router.use('/terrenos', require('./terrenos'));
router.use('/intereses', require('./intereses'));
router.use('/consultas',require('./consultas'));
router.use('/apartados',require('./apartados'));


module.exports = router;