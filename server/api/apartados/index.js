import controller from './apartados.controller';
var router = require('express').Router();

router.route('/')
.post(controller.createApartado)
.get(controller.findApartado)
.put(controller.modifyApartado)

router.route('/delete')
.put(controller.deleteApartado)

router.route('/model')
.delete(controller.resetApartado)

router.route('/relation')
.post(controller.setConsulta)
.put(controller.getConsulta)
.delete(controller.deleteConsulta)



module.exports = router;