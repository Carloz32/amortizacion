import Interes from '../intereses/intereses.model';
import Lote from '../lotes/lotes.model';
import Apartado from '../apartados/apartados.model';
require('lodash');
//WARNIG: IT WILL DROP THE TABLE
exports.resetApartado = (req, res) => {
  Apartado.sync({ force: true })
    .then(() => {
      // Table created
      res.sendStatus(200);
    });
}

exports.createApartado = (req, res) => {
  /* console.log ('Si entra aqui');
   Lote.findOne({
       where: { nombre: req.body.nombre }
     })
   .then((terreno) => {
     if (terreno) {
       res.status(200)
       .send('El terreno ya existe');
     }
     else {
       Terreno.create(req.body)
       .then(() => {
         res.sendStatus(200);
       })
       .catch((err) => {
         console.log(err);
         res.sendStatus(500);
       })
     }
   })
 }/ */


  Apartado.create(req.body)
    .then(() => {
      res.sendStatus(200);
    })
    .catch((err) => {
      console.log(err);
      res.sendStatus(500);
    });
}

exports.modifyApartado = (req, res) => {/*
    Lote.findOne({
        where: { nombre: req.body.nombre }
      })
    .then((Terreno) => {
      if (Terreno) {
        res.status(200)
        .send('El usuario ya existe');
      }
      else {
       */ Apartado.findOne({
    where: { id: req.body.id }
  })
    .then((Apartado) => {
      if (Apartado) {
        Apartado.update(req.body)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
  /* }
 })*/
}

exports.deleteApartado = (req, res) => {
  Apartado.findOne({
    where: { id: req.body.id }
  })
    .then((apartado) => {
      if (apartado) {
        apartado.destroy()
          .then(apartado => {
            res.status(200)
              .json(apartado.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.findApartado = (req, res) => {
  Apartado.findAll()
    .then((apartados) => {
      res.send(apartados);
    })
}


exports.setConsulta = (req, res) => {

  Apartado.findOne({
    where: { id: req.body.id }
  })
    .then((apartado) => {
      if (apartado) {
        apartado.setConsulta(req.body.ConsultaId)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.getConsulta = (req, res) => {

  Apartado.findOne({
    where: { id: req.body.id }
  }).then((apartado) => {

    apartado.getConsulta()
      .then((consulta) => {
        res.send(consulta);
      })

  })
}

exports.deleteConsulta = (req, res) => {
  Apartado.findOne({
    where: { id: req.body.id }
  })
    .then((apartado) => {
      if (apartado) {
        apartado.setConsulta(null)
          .then(apartado => {
            res.status(200)
              .json(apartado.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })

}