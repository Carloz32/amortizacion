import sequelizeDb from '../../config/db';
import Sequelize from 'sequelize';
import Consulta from '../consultas/consultas.model'

var Apartado = sequelizeDb.define('Apartado', {
    pathINE: { //Path del INE
        allowNull: false,
        type: Sequelize.STRING
    },
    pathComprobante: {//Path del comprobante
        allowNull: false,
        type: Sequelize.STRING
    },
    datosPersonales: {//Numero de pagos a realizar 
        allowNull: false,
        type: Sequelize.STRING
    },
    
});
Apartado.belongsTo(Consulta, {as:'Consulta'}); 
//Apartado.belongsTo(Interes,{as:'Interes'});


module.exports = Apartado;