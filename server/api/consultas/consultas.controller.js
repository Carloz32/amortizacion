import Interes from '../intereses/intereses.model';
import Lote from '../lotes/lotes.model';
import Consulta from '../consultas/consultas.model';
require('lodash');
//WARNIG: IT WILL DROP THE TABLE
exports.resetConsulta = (req, res) => {
  Consulta.sync({ force: true })
    .then(() => {
      // Table created
      res.sendStatus(200);
    });
}

exports.createConsulta = (req, res) => {
  /* console.log ('Si entra aqui');
   Lote.findOne({
       where: { nombre: req.body.nombre }
     })
   .then((terreno) => {
     if (terreno) {
       res.status(200)
       .send('El terreno ya existe');
     }
     else {
       Terreno.create(req.body)
       .then(() => {
         res.sendStatus(200);
       })
       .catch((err) => {
         console.log(err);
         res.sendStatus(500);
       })
     }
   })
 }/ */


  Consulta.create(req.body)
    .then(() => {
      res.sendStatus(200);
    })
    .catch((err) => {
      console.log(err);
      res.sendStatus(500);
    });
}

exports.modifyConsulta = (req, res) => {/*
    Lote.findOne({
        where: { nombre: req.body.nombre }
      })
    .then((Terreno) => {
      if (Terreno) {
        res.status(200)
        .send('El usuario ya existe');
      }
      else {
       */ Consulta.findOne({
    where: { id: req.body.id }
  })
    .then((Consulta) => {
      if (Consulta) {
        Consulta.update(req.body)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
  /* }
 })*/
}

exports.deleteConsulta = (req, res) => {
  Consulta.findOne({
    where: { id: req.body.id }
  })
    .then((consulta) => {
      if (consulta) {
        consulta.destroy()
          .then(consulta => {
            res.status(200)
              .json(consulta.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.findConsulta = (req, res) => {
  Consulta.findAll()
    .then((consultas) => {
      res.send(consultas);
    })
}

exports.setLote = (req, res) => {

  Consulta.findOne({
    where: { id: req.body.id }
  })
    .then((Consulta) => {
      if (Consulta) {
        Consulta.setLote(req.body.LoteId)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.getLote = (req, res) => {

  Consulta.findOne({
    where: { id: req.body.id }
  }).then((consulta) => {

    consulta.getLote()
      .then((lote) => {
        res.send(lote);
      })

  })
}

exports.deleteLote = (req, res) => {
  Consulta.findOne({
    where: { id: req.body.id }
  })
    .then((consulta) => {
      if (consulta) {
        consulta.setLote(null)
          .then(consulta => {
            res.status(200)
              .json(consulta.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })

}

exports.setInteres = (req, res) => {

  Consulta.findOne({
    where: { id: req.body.id }
  })
    .then((Consulta) => {
      if (Consulta) {
        Consulta.setInteres(req.body.InteresId)
          .then(() => {
            res.sendStatus(200)
          })
          .catch((err) => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })
}

exports.getInteres = (req, res) => {

  Consulta.findOne({
    where: { id: req.body.id }
  }).then((consulta) => {

    consulta.getInteres()
      .then((interes) => {
        res.send(interes);
      })

  })
}

exports.deleteInteres = (req, res) => {
  Consulta.findOne({
    where: { id: req.body.id }
  })
    .then((consulta) => {
      if (consulta) {
        consulta.setInteres(null)
          .then(consulta => {
            res.status(200)
              .json(consulta.dataValues);
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(500);
          })
      }
      else {
        res.sendStatus(404);
      }
    })

}