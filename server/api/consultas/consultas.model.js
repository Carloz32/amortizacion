import sequelizeDb from '../../config/db';
import Sequelize from 'sequelize';
import Lote from '../lotes/lotes.model'
import Interes from '../intereses/intereses.model';

var Consulta = sequelizeDb.define('Consulta', {
    montoInicial: { //Deudad inicial
        allowNull: false,
        type: Sequelize.FLOAT
    },
    pagoMensual: {//Pago mensual generado
        allowNull: false,
        type: Sequelize.FLOAT
    },
    periodo: {//Numero de pagos a realizar 
        allowNull: false,
        type: Sequelize.INTEGER
    },
    
});
Consulta.belongsTo(Lote, {as:'Lote'}); 
Consulta.belongsTo(Interes,{as:'Interes'});


module.exports = Consulta;