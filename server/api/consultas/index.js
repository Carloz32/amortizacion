import controller from './consultas.controller';
var router = require('express').Router();

router.route('/')
.post(controller.createConsulta)
.get(controller.findConsulta)
.put(controller.modifyConsulta)

router.route('/delete')
.put(controller.deleteConsulta)

router.route('/model')
.delete(controller.resetConsulta)

//Metodos para la relacion de Lote
router.route('/relation')
.post(controller.setLote)
.put(controller.getLote)
.delete(controller.deleteLote)

//Metodos para la relacion de Consulta
router.route('/relationInteres')
.post(controller.setInteres)
.put(controller.getInteres)
.delete(controller.deleteInteres)


module.exports = router;