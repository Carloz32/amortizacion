import express  from 'express';
import sequelize from './config/db';
import bodyParser from 'body-parser';

const app = express();
app.use(bodyParser.json({limit: '200mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '200mb', extended: true}));


sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });



  app.use('/api/', require('./api/'));
  
  app.listen(8080
    , function () {
    console.log('Example app listening on port 8080!');
  });
//routes


